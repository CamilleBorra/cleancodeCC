const MAPPING_VALUE_FIGURE={
    "brelan": 28,
    "carre" : 35,
    "suite" : 40,
    "yams"  : 50
}
const rolling = (tabDice) =>{
   
    let score = 0
    tabDice = tabDice.sort(function(a, b) {
        return a - b;
    })

    if (tabDice.join("") === "12345" || tabDice.join("") === "23456") {
        score += MAPPING_VALUE_FIGURE['suite'];
    }
    for (let diceValue = 1; diceValue <= 6; diceValue++) {
        const sameDice = tabDice.filter(d => d === diceValue).length;
        if (sameDice == 5) {
            score += MAPPING_VALUE_FIGURE['yams'];
            break
        }
        if (sameDice == 4) {
            score += MAPPING_VALUE_FIGURE['carre'];
            break
        }
        if (sameDice == 3) {
            score += MAPPING_VALUE_FIGURE['brelan'];
            break
        }
    }
    if(score == 0){
        score = tabDice[0] + tabDice[1] + tabDice[2] + tabDice[3] + tabDice[4]

    }
    return score
}

module.exports = rolling