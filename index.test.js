const rolling = require("./index")
describe("Rolling dice", () => {
    it("should return 28 when provided [1,1,1,2,2]", () =>{
        expect(rolling([1,1,1,2,2])).toBe(28)
    })
    it("should return 28 when provided [2,6,2,6,2]", () =>{
        expect(rolling([2,6,2,6,2])).toBe(28)
    })
    it("should return 35 when provided [2,2,2,6,2]", () =>{
        expect(rolling([2,2,2,6,2])).toBe(35)
    })
    it("should return 50 when provided [2,2,2,2,2]", () =>{
        expect(rolling([2,2,2,2,2])).toBe(50)
    })
    it("should return 40 when provided [1,3,4,2,5]", () =>{
        expect(rolling([1,3,4,2,5])).toBe(40)
    })
    it("should return 16 when provided [1,4,4,2,5]", () =>{
        expect(rolling([1,4,4,2,5])).toBe(16)
    })
})